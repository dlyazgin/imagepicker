package com.esafirm.imagepicker.features;

import android.content.Context;
import android.view.View;

import com.esafirm.imagepicker.R;
import com.marcoscg.dialogsheet.DialogSheet;

public class MediaDialogFragment {
    public static MediaDialogFragment getInstance(Context context, Listener listener){
        return new MediaDialogFragment(context, listener);
    }

    private DialogSheet dialogSheet;

    private MediaDialogFragment(Context context, Listener listener){
        View view = View.inflate(context, R.layout.ef_dialog_media_picker, null);
        dialogSheet = new DialogSheet(context)
                .setView(view)
                .setCancelable(true)
                .setNegativeButton(android.R.string.cancel, null)
                .setButtonsColorRes(R.color.ef_colorPrimary);

        View takePhotoLayout = view.findViewById(R.id.takePhotoLayout);
        View takeVideoLayout = view.findViewById(R.id.takeVideoLayout);

        takePhotoLayout.setOnClickListener(v -> {
            listener.onTakePhoto();
            dialogSheet.dismiss();
        });

        takeVideoLayout.setOnClickListener(v -> {
            listener.onTakeVideo();
            dialogSheet.dismiss();
        });
    }

    public void show(){
        dialogSheet.show();
    }

    public interface Listener{
        void onTakePhoto();
        void onTakeVideo();
    }
}
